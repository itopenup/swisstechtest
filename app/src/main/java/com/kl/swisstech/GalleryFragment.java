package com.kl.swisstech;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class GalleryFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
    public static final String FRAGMENT_NAME = "gallery";

    private static final int PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 0;
    private static final int LOADER_GALLERY_LIST = 0;
    private static final int LOADER_GALLERY_THUMBNAILS = 1;

    private static final int HANDLER_WHAT_START_PROGRESS = 0;
    private static final int HANDLER_WHAT_INCREMENT_PROGRESS = 1;
    private static final int HANDLER_WHAT_FINISH_PROGRESS = 2;
    private static final int HANDLER_WHAT_LOAD_THUMBNAILS = 3;

    private RecyclerView mGalleryListView;
    private ProgressBar mProgressBarView;
    private List<GalleryListAdapter.GalleryListItem> mGalleryList;
    private GalleryListAdapter mGalleryListAdapter;

    /* Should create a static class extended Handler but in the next release */
    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case HANDLER_WHAT_START_PROGRESS:
                    mProgressBarView.setMax(msg.arg1);
                    mProgressBarView.setProgress(0);
                    setProgressBarVisibility(View.VISIBLE);
                    break;

                case HANDLER_WHAT_FINISH_PROGRESS:
                    setProgressBarVisibility(View.GONE);
                    mGalleryListAdapter.notifyDataSetChanged();
                    break;

                case HANDLER_WHAT_INCREMENT_PROGRESS:
                    mProgressBarView.incrementProgressBy(1);
                    break;

                case HANDLER_WHAT_LOAD_THUMBNAILS:
                    getActivity()
                            .getSupportLoaderManager()
                            .initLoader(LOADER_GALLERY_THUMBNAILS, null, GalleryFragment.this);
                    break;
            }
        }
    };

    public GalleryFragment() {
        mGalleryList = Collections.synchronizedList(
                new ArrayList<GalleryListAdapter.GalleryListItem>());
    }

    public static GalleryFragment newInstance() {
        return new GalleryFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_galery, container, false);
        mProgressBarView = (ProgressBar) view.findViewById(R.id.progress);
        mGalleryListView = (RecyclerView) view.findViewById(R.id.gallery_list);
        mGalleryListView.setLayoutManager(new LinearLayoutManager(getActivity()));

        mGalleryListAdapter = new GalleryListAdapter(getActivity(), mGalleryList);
        mGalleryListView.setAdapter(mGalleryListAdapter);

        setHasOptionsMenu(true);

        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
        } else {
            getActivity()
                    .getSupportLoaderManager()
                    .initLoader(LOADER_GALLERY_LIST, null, this);
        }

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.gallery_list, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sync:
                getActivity()
                        .getSupportLoaderManager()
                        .restartLoader(LOADER_GALLERY_LIST, null, this);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getActivity()
                            .getSupportLoaderManager()
                            .initLoader(LOADER_GALLERY_LIST, null, this);
                } else {
                    Toast.makeText(getActivity(),
                            R.string.read_external_storage_permission_denied,
                            Toast.LENGTH_SHORT).show();
                }
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case LOADER_GALLERY_LIST:
                return new CursorLoader(getActivity(),
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        new String[] {
                                MediaStore.Images.Media._ID,
                                MediaStore.Images.Media.DATA,
                                MediaStore.Images.Media.DISPLAY_NAME,
                                MediaStore.Images.Media.TITLE,
                                MediaStore.Images.Media.SIZE
                        },
                        null,
                        null,
                        null);

            case LOADER_GALLERY_THUMBNAILS:
                return new CursorLoader(getActivity(),
                        MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI,
                        new String[] {
                                MediaStore.Images.Thumbnails.IMAGE_ID,
                                MediaStore.Images.Thumbnails.DATA
                        },
                        MediaStore.Images.Thumbnails.KIND + "=" +
                                MediaStore.Images.Thumbnails.MINI_KIND,
                        null,
                        null);
        }

        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, final Cursor data) {
        switch (loader.getId()) {
            case LOADER_GALLERY_LIST:
                mGalleryList.clear();

                if (data != null && data.getCount() > 0) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                handler.sendMessage(handler.obtainMessage(HANDLER_WHAT_START_PROGRESS,
                                        data.getCount(), 0));

                                data.moveToFirst();

                                int columnIndexId = data.getColumnIndex(MediaStore.Images.Media._ID);
                                int columnIndexName = data.getColumnIndex(MediaStore.Images.Media.DISPLAY_NAME);
                                int columnIndexTitle = data.getColumnIndex(MediaStore.Images.Media.TITLE);
                                int columnIndexSize = data.getColumnIndex(MediaStore.Images.Media.SIZE);
                                int columnIndexPath = data.getColumnIndex(MediaStore.Images.Media.DATA);

                                while (!data.isAfterLast()) {
                                    GalleryListAdapter.GalleryListItem item = new GalleryListAdapter.GalleryListItem();
                                    item.setId(data.getInt(columnIndexId));
                                    item.setTitle(data.getString(columnIndexName));
                                    item.setSize(data.getLong(columnIndexSize));
                                    item.setImgPath(data.getString(columnIndexPath));
                                    item.setMd5(getFileMD5(data.getString(columnIndexPath)));
                                    mGalleryList.add(item);

                                    data.moveToNext();

                                    handler.sendEmptyMessage(HANDLER_WHAT_INCREMENT_PROGRESS);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            } finally {
                                handler.sendEmptyMessage(HANDLER_WHAT_FINISH_PROGRESS);
                                handler.sendEmptyMessage(HANDLER_WHAT_LOAD_THUMBNAILS);
                            }
                        }
                    }).start();
                }
                break;

            case LOADER_GALLERY_THUMBNAILS:
                if (data != null && data.getCount() > 0) {
                    data.moveToFirst();

                    int columnIndexId = data.getColumnIndex(MediaStore.Images.Thumbnails.IMAGE_ID);
                    int columnIndexPath = data.getColumnIndex(MediaStore.Images.Thumbnails.DATA);

                    while (!data.isAfterLast()) {
                        for (GalleryListAdapter.GalleryListItem item : mGalleryList) {
                            if (item.getId() == data.getInt(columnIndexId)) {
                                item.setThumbPath(data.getString(columnIndexPath));
                                break;
                            }
                        }

                        data.moveToNext();
                    }

                    mGalleryListAdapter.notifyDataSetChanged();
                }

                break;
        }
    }

    @Override
    public void onLoaderReset(Loader loader) {

    }

    private String getFileMD5(String path) {
        File file = new File(path);
        FileInputStream fileInputStream;
        byte[] bytes = new byte[2048];
        int numBytes;

        MessageDigest md;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }

        md.reset();

        try {
            fileInputStream = new FileInputStream(file);
            while ((numBytes = fileInputStream.read(bytes)) != -1) {
                md.update(bytes, 0, numBytes);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        byte[] digest = md.digest();
        StringBuilder hexString = new StringBuilder();

        for (byte b : digest) {
            String hex = Integer.toHexString(0xFF & b);
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }

        return hexString.toString();
    }

    private void setProgressBarVisibility(int visibility) {
        if (visibility == View.GONE) {
            mProgressBarView.setVisibility(View.GONE);
            mGalleryListView.setVisibility(View.VISIBLE);
        } else {
            mProgressBarView.setVisibility(View.VISIBLE);
            mGalleryListView.setVisibility(View.GONE);
        }
    }
}
