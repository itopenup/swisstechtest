package com.kl.swisstech;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

public class GalleryListAdapter extends RecyclerView.Adapter<GalleryListAdapter.ViewHolder> {

    private List<GalleryListItem> galleryList;
    private Context context;

    public GalleryListAdapter(Context context, List<GalleryListItem> galleryList) {
        this.galleryList = galleryList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.list_item_gallery, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int i) {
        if (i < 0 || i > getItemCount()) {
            return;
        }

        holder.title.setText(galleryList.get(i).getTitle());
        holder.size.setText(Formatter.formatShortFileSize(context, galleryList.get(i).getSize()));

        String path = galleryList.get(i).getThumbPath();
        if (path != null) {
            Bitmap bmp = BitmapFactory.decodeFile(path);
            holder.img.setImageBitmap(bmp);
        } else {
            holder.img.setImageResource(R.mipmap.ic_launcher);
        }

        holder.md5.setText(galleryList.get(i).getMd5());
    }

    @Override
    public int getItemCount() {
        return galleryList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        private TextView title;
        private TextView size;
        private TextView md5;
        private ImageView img;

        ViewHolder(View view) {
            super(view);

            title = (TextView)view.findViewById(R.id.title);
            size = (TextView)view.findViewById(R.id.size);
            md5 = (TextView)view.findViewById(R.id.md5);
            img = (ImageView) view.findViewById(R.id.img);
        }
    }

    public static class GalleryListItem {
        String title;
        String md5;
        String imgPath;
        String thumbPath;
        long size;
        int id;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getMd5() {
            return md5;
        }

        public void setMd5(String md5) {
            this.md5 = md5;
        }

        public long getSize() {
            return size;
        }

        public void setSize(long size) {
            this.size = size;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getImgPath() {
            return imgPath;
        }

        public void setImgPath(String imgPath) {
            this.imgPath = imgPath;
        }

        public String getThumbPath() {
            return thumbPath;
        }

        public void setThumbPath(String thumbPath) {
            this.thumbPath = thumbPath;
        }
    }
}
